## qssi-user 11 RKQ1.200826.002 V12.5.8.0.RGACNXM release-keys
- Manufacturer: xiaomi
- Platform: kona
- Codename: thyme
- Brand: Xiaomi
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.200826.002
- Incremental: V12.5.8.0.RGACNXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: Xiaomi/thyme/thyme:11/RKQ1.200826.002/V12.5.8.0.RGACNXM:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.200826.002-V12.5.8.0.RGACNXM-release-keys
- Repo: xiaomi_thyme_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
